import minimalmodbus
import random
import time
import sys
import traceback
import struct

DESCRIPTION = """
Python script for testing NBB joystick used with SENTRY AUV.

Attempts to either read the joystic state (--test='read')
or write random values for the 3 displayed variables (--test='write').


The --rate parameter adjusts the loop frequency.  SENTRY typically uses
a 4Hz polling rate, but here the default is 1Hz.


Use the --no-hardware option to just print what values would be displayed on the
joystick.  Really only useful while developing this test w/o access to any
hardware.
"""

DEFAULT_SERIAL = '/dev/ttyUSB0'
DEFAULT_RATE_HZ = 1
DEFAULT_TIMEOUT_SECS = 1.0

READ_TEST_TYPE = 'read'
WRITE_TEST_TYPE = 'write'
DEFAULT_TEST = READ_TEST_TYPE

def main():
    import argparse


    parser = argparse.ArgumentParser(description=DESCRIPTION)
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--no-hardware', action='store_true', help="just print values to stdout")
    group.add_argument('--port', type=str, help='serial port', default=DEFAULT_SERIAL)
    parser.add_argument('--test', type=str, choices=['read', 'write'], help='test type', default=DEFAULT_TEST)
    parser.add_argument('--rate', type=float, help='repitition rate (in Hz)', default=DEFAULT_RATE_HZ)
    parser.add_argument('--loops', type=int, help='number of loops', default=-1)
    args = parser.parse_args()

    if not args.no_hardware:
        bus = minimalmodbus.Instrument(args.port, 0x03, mode='rtu', debug=True)
        bus.serial.baudrate = 19200
        bus.parity = 'N'
        bus.bytesize = 8
        bus.stopbits = 1
        bus.write_timeout = DEFAULT_TIMEOUT_SECS
        bus.timeout = DEFAULT_TIMEOUT_SECS

    test = args.test
    rate = 1.0 / args.rate
    
    loopnum = 0
    succeeded = 0

    while True:
        loopnum += 1
        if test == WRITE_TEST_TYPE:
            hdg = random.randint(0, 3599)
            rate_ = random.randint(-2000, 2000)
            ship = random.randint(0, 3599)
            print("setting HDG: %.1f, RATE: %.2f, SHIP: %.1f" %(hdg * .1, rate_ * .01, ship * .1,))
            rate_ = struct.unpack(">H", struct.pack(">h", rate_))[0]
            if not args.no_hardware:
                try:
                    bus.write_registers(0x04, [hdg, rate_, ship])
                    succeeded += 1
                except minimalmodbus.NoResponseError as e:
                    traceback.print_exception(e, file=sys.stdout)
            else:
                succeeded += 1
        else:
            if not args.no_hardware:
                try:
                    bus.read_string(0x11, 8)
                    succeeded += 1
                except minimalmodbus.NoResponseError as e:
                    traceback.print_exception(e, file=sys.stdout)
            else:
                succeeded += 1
        time.sleep(rate)
        print("%d of %d tests successful (%.2f)" %(succeeded, loopnum, succeeded * 100.0 / loopnum))
        if args.loops > 0 and loopnum == args.loops:
            break

if __name__ == '__main__':
    main()
