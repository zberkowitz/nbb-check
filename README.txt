Simple test script that poll the NBB "Compact-M" transceiver for the SENTRY AUV.


Requires the "minimalmodbus" python package:
https://pypi.org/project/minimalmodbus/

Examples:
Attempt to read the joystick registers 100 times at a rate of 1Hz:
    nbb-check.py --test read --loops 100 --rate 1

Attempt to write the joystick display registers 50 times at a rate of 4Hz:
    nbb-check.py --test write --loops 50 --rate 4
